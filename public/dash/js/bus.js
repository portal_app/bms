$(function() {
    var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#BusTable').DataTable({
        ajax: {
            url: _link +  'feeds/json/buses',
            dataSrc: ''
        },
        columns: [
            { data: 'id', name: 'id'},
            { data: 'code', name: 'code' },
            { data: 'driver_id', name: 'driver_id' },
            { data: 'park', name: 'park' },
            { data: 'type', name: 'type' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: '', name: '' },
        ],
        columnDefs: [
            {
                targets: -1, data: null, defaultContent: buttons
            }
        ],
    });

    $(document).on('click', '#BusTable .btnModify', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/bus/' + id + '/edit';
    });

    $(document).on('click', '#BusTable .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this bus?')) {
            $.post(_link + 'dash/bus/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

    $(document).on('click', '#BusTable td:not(td:has(.btnDelete))', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/bus/' + id;
    });

});
