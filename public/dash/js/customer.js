$(function() {
    var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#customerTable').DataTable({
        ajax: {
            url: _link +  'feeds/json/customers',
            dataSrc: ''
        },
        columns: [
            // { data: 'id', name: 'id'},
            // { data: 'firstname', name: 'firstname' },
            // { data: 'lastname', name: 'lastname' },
            { data: 'username', name: 'username' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            // { data: 'created_at', name: 'created_at' },
            // { data: 'updated_at', name: 'updated_at' },
            { data: '', name: '' },
        ],
        columnDefs: [
            {
                targets: -1, data: null, defaultContent: buttons
            }
        ],
    });

    $(document).on('click', '#customerTable .btnModify', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/customer/' + id + '/edit';
    });

    $(document).on('click', '#customerTable .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this user?')) {
            $.post(_link + 'dash/customer/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

    $(document).on('click', '#customerTable td:not(td:has(.btnDelete))', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/customer/' + id;
    });

});
