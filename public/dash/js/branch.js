$(function() {
    var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#tblBranches').DataTable({
        ajax: {
            url: _link +  'feeds/json/branches',
            dataSrc: ''
        },
        columns: [
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'control_by', name: 'control_by' },
            { data: 'phone', name: 'phone' },
            { data: '', name: '' },
        ],
        columnDefs: [
            {
                targets: -1, data: null, defaultContent: buttons
            }
        ],
    });

    $(document).on('click', '#tblBranches .btnModify', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/branch/' + id + '/edit';
    });

    $(document).on('click', '#tblBranches .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this user?')) {
            $.post(_link + 'dash/branch/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

    $(document).on('click', '#tblBranches td:not(td:has(.btnDelete))', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/branch/' + id;
    });

});
