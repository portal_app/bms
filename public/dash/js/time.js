$(function() {
	var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#timeTable').DataTable({
        ajax: {
        	url: _link + 'feeds/json/times',	//'{!! route('dash.time.json') !!}',
        	dataSrc: ''
    	},
        columns: [
            { data: 'id', name: 'id'},
            { data: 'time', name: 'time' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: '', name: '' },
        ],
        columnDefs: [
        	{
        		targets: -1, data: null, defaultContent: buttons
        	}
        ],
    });

	$(document).on('click', '#timeTable .btnModify', function(){
	    var tr = $(this).parents('tr');
	    var id = table.row(tr).data().id;
	    window.location.href = _link + 'dash/time/' + id + '/edit';
	});

    $(document).on('click', '#timeTable .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this time?')) {
            $.post(_link + 'dash/time/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

	$(document).on('click', '#timeTable td:not(td:has(.btnDelete))', function(){
	    var tr = $(this).parents('tr');
	    var id = table.row(tr).data().id;
	    window.location.href = _link + 'dash/time/' + id;
	});

	$(document).on('click', '.btn-create-new-time', function(){
		window.location.href = _link + 'dash/time/create';	
	})
});
    