$(function() {
	var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#placeTable').DataTable({
        ajax: {
        	url: _link + 'feeds/json/places',	//'{!! route('dash.place.json') !!}',
        	dataSrc: ''
    	},
        columns: [
            { data: 'id', name: 'id'},
            { data: 'name', name: 'name' },
            { data: 'code', name: 'code' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: '', name: '' },
        ],
        columnDefs: [
        	{
        		targets: -1, data: null, defaultContent: buttons
        	}
        ],
    });

	$(document).on('click', '#placeTable .btnModify', function(){
	    var tr = $(this).parents('tr');
	    var id = table.row(tr).data().id;
	    window.location.href = _link + 'dash/place/' + id + '/edit';
	});

    $(document).on('click', '#placeTable .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this place?')) {
            $.post(_link + 'dash/place/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

	$(document).on('click', '#placeTable td:not(td:has(.btnDelete))', function(){
	    var tr = $(this).parents('tr');
	    var id = table.row(tr).data().id;
	    window.location.href = _link + 'dash/place/' + id;
	});

	$(document).on('click', '.btn-create-new-place', function(){
		window.location.href = _link + 'dash/place/create';	
	})
});
