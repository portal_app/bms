$(function() {
    var buttons = '<span class="text-center"><button class="btn btn-info btn-xs btnModify" data-title="Modify"><span class="glyphicon glyphicon-pencil"></span></button>';
            buttons += ' <button class="btn btn-danger btn-xs btnDelete" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></button></span>';

    var table = $('#DriverTable').DataTable({
        ajax: {
            url: _link +  'feeds/json/drivers',
            dataSrc: ''
        },
        columns: [
            { data: 'id', name: 'id'},
            { data: 'firstname', name: 'firstname' },
            { data: 'lastname', name: 'lastname' },
            { data: 'phone', name: 'phone' },
            { data: 'email', name: 'email' },
            { data: 'address', name: 'address' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: '', name: '' },
        ],
        columnDefs: [
            {
                targets: -1, data: null, defaultContent: buttons
            }
        ],
    });

    $(document).on('click', '#DriverTable .btnModify', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/driver/' + id + '/edit';
    });

    $(document).on('click', '#DriverTable .btnDelete', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        if (confirm('Do you really want to delete this driver?')) {
            $.post(_link + 'dash/driver/' + id, {'_method':'delete', '_token':_code}, function(result){
                table.row(tr).node().remove();
            }, 'json');
        }
    });

    $(document).on('click', '#DriverTable td:not(td:has(.btnDelete))', function(){
        var tr = $(this).parents('tr');
        var id = table.row(tr).data().id;
        window.location.href = _link + 'dash/driver/' + id;
    });

});
