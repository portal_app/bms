var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles('bootstrap.css', 'public/css/bootstrap.css');
    mix.styles('jquery-ui.css', 'public/css/jquery-ui.css');
    mix.styles('form.css', 'public/css/form.css');
    mix.styles('etalage.css', 'public/css/etalage.css');
    mix.styles('megamenu.css', 'public/css/megamenu.css');
    mix.styles('style.css', 'public/css/style.css');

    mix.scripts('jquery-1.11.1.min.js', 'public/js/jquery-1.11.1.min.js');
    mix.scripts('bootstrap.min.js', 'public/js/bootstrap.min.js');
    mix.scripts('jquery.easydropdown.js', 'public/js/jquery.easydropdown.js');
    mix.scripts('jquery.etalage.min.js', 'public/js/jquery.etalage.min.js');
    mix.scripts('jquery.flexisel.js', 'public/js/jquery.flexisel.js');
    mix.scripts('jquery.jscrollpane.min.js', 'public/js/jquery.jscrollpane.min.js');
    mix.scripts('megamenu.js', 'public/js/megamenu.js');
    mix.scripts('menu_jquery.js', 'public/js/menu_jquery.js');
    mix.scripts('responsiveslides.min.js', 'public/js/responsiveslides.min.js');
    mix.scripts('simpleCart.min.js', 'public/js/simpleCart.min.js');
    mix.scripts('jquery-ui.min.js', 'public/js/jquery-ui.min.js');


    // Dash Assets


    mix.styles('css/bootstrap-theme.min.css', 'public/dash/css/bootstrap-theme.min.css','themes/dash/bower_components/bootstrap/dist');
    mix.styles('css/bootstrap.min.css', 'public/dash/css/bootstrap.min.css','themes/dash/bower_components/bootstrap/dist');
    mix.styles('metisMenu.min.css', 'public/dash/css/metisMenu.min.css','themes/dash/bower_components/metisMenu/dist');
    mix.styles('css/timeline.css', 'public/dash/css/timeline.css','themes/dash/dist');
    mix.styles('css/sb-admin-2.css', 'public/dash/css/sb-admin-2.css','themes/dash/dist');
    mix.styles('morris.css','public/dash/css/morris.css','themes/dash/bower_components/morrisjs');
    mix.styles('css/font-awesome.min.css','public/dash/css/font-awesome.min.css','themes/dash/bower_components/font-awesome');

    mix.scripts('jquery.min.js','public/dash/js/jquery.min.js','themes/dash/bower_components/jquery/dist');
    mix.scripts('js/bootstrap.min.js','public/dash/js/bootstrap.min.js','themes/dash/bower_components/bootstrap/dist');
    mix.scripts('metisMenu.min.js','public/dash/js/metisMenu.min.js','themes/dash/bower_components/metisMenu/dist');
    mix.scripts('raphael-min.js','public/dash/js/raphael-min.js','themes/dash/bower_components/raphael');
    mix.scripts('morris.min.js','public/dash/js/morris.min.js','themes/dash/bower_components/morrisjs');
    mix.scripts('morris-data.js','public/dash/js/morris-data.js','themes/dash/js');
    mix.scripts('sb-admin-2.js','public/dash/js/sb-admin-2.js','themes/dash/dist/js');




    mix.styles([

        //'datatables/media/css/jquery.dataTables.min.css',
        'datatables/media/css/dataTables.bootstrap.min.css',
        'datatables-plugins/integration/font-awesome/dataTables.fontAwesome.css'

    ], 'public/dash/css/datatables.css', 'themes/dash/bower_components');

    mix.scripts([

        'datatables/media/js/jquery.dataTables.min.js',
        'datatables/media/js/dataTables.bootstrap.min.js',
        //'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'

    ], 'public/dash/js/datatables.js', 'themes/dash/bower_components');

    mix.styles([
        'bootstrap-clockpicker.min.css',
    ], 'public/dash/css/bootstrap-clockpicker.min.css', 'themes/dash/bower_components/jQuery-Clock-Style-Time-Picker-Plugin-For-Bootstrap-3-clockpicker/dist');

    mix.scripts([
        'bootstrap-clockpicker.min.js'
    ], 'public/dash/js/bootstrap-clockpicker.min.js', 'themes/dash/bower_components/jQuery-Clock-Style-Time-Picker-Plugin-For-Bootstrap-3-clockpicker/dist');

});