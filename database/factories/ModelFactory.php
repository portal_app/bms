<?php
// Use App\Models\Branch;
// Use App\Models\Bus;
// Use App\Models\Driver;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

// $factory->define(App\User::class, function (Faker\Generator $faker) {
//     return [
//         'name' => $faker->name,
//         'email' => $faker->email,
//         'password' => bcrypt(str_random(10)),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\Models\Driver::class, function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->name($gender = null | 'male' | 'female'),
        'lastname'  => $faker->lastName,
        'phone'     => $faker->numerify('0## ### ###'),
        'email'     => $faker->email,
        'address'   => $faker->Address,
        'card_no'   => $faker->numberBetween($min = 1000000000, $max = 9999999999)
    ];
});

$factory->define(App\Models\Branch::class, function (Faker\Generator $faker) {
    return [
        'name'       => $faker->name,
        'address'    => $faker->Address,
        'control_by' => 1,
        'phone'      => $faker->numerify('0## ### ###')
    ];
});
$factory->define(App\Models\Bus::class, function (Faker\Generator $faker) {
    return [
        'code'      => $faker->bothify('#?? ####'),
        // 'driver_id' => 'factory:Driver',
        'driver_id' => 4,
        'park'      => $faker->sentence,
        'type'      => $faker->numberBetween($min = 1, $max = 9)
    ];
});
