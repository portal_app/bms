<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class TblDrivers extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tbl_drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 55);
            $table->string('lastname', 55);
            $table->string('phone', 11)->unique();
            $table->string('email')->unique();
            $table->text('address');
            $table->integer('card_no')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tbl_drivers');
    }
}
