<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class TblUsers extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tbl_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 55);
            $table->string('lastname', 55);
            $table->string('username', 55);
            $table->string('email')->unique();
            $table->string('phone', 11)->unique();
            $table->text('address');
            $table->string('card_no', 9);
            $table->enum('type', ['manager', 'admin', 'user']);
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tbl_users');
    }
}
