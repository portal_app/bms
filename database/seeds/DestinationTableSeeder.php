<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DestinationTableSeeder extends Seeder {

    public function run()
    {

          $places = [
            'Kampot',
            'Keb',
            'Preah Sihanouk',
            'Kampong Chhnang',
            'Posat',
            'Battambong',
            'Phnom Penh Thmei',
            'Pailin',
            'Banteay Meanchey',
            'Siem Reap',
            'Kompong Thom',
            'Kompong Cham',
            'Prey Veng',
            'Svay Reang',
            'Takao',
            'Kandal',
            'Kompong Speu',
            'Krotie',
            'Streung Treng',
            'Mondul Kiri',
            'Preah Vihea',
            'Rattanak Kiri',
            'Koh Kong',
            'Oudor Meanchey'
        ];

        $prices = [10000, 20000, 30000, 40000, 50000, 60000, 70000];

        foreach ($places as $i => $place) {
            DB::table('tbl_destinations')->insert([
                'place_from' => 'Phnom Penh',
                'place_to' => $place,
                'departure_id' => rand(0,5),
                'price' => $prices[ rand(0,6) ],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }



        // DB::table('tbl_destinations')->insert([
        //     'place_from' => 1,
        //     'place_to' => 2,
        //     'departure_id' => 1,
        //     'price' => 30000,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('tbl_destinations')->insert([
        //     'place_from' => 1,
        //     'place_to' => 3,
        //     'departure_id' => 1,
        //     'price' => 25000,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('tbl_destinations')->insert([
        //     'place_from' => 1,
        //     'place_to' => 4,
        //     'departure_id' => 1,
        //     'price' => 20000,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('tbl_destinations')->insert([
        //     'place_from' => 1,
        //     'place_to' => 5,
        //     'departure_id' => 1,
        //     'price' => 10000,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
        // DB::table('tbl_destinations')->insert([
        //     'place_from' => 1,
        //     'place_to' => 6,
        //     'departure_id' => 1,
        //     'price' => 40000,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);
    }
}
