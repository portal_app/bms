<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PlaceTableSeeder extends Seeder {

    public function run() {
/*
        24 provinces with general post code
        reference link:
            http://www.stat.go.jp/info/meetings/cambodia/pdf/00dis_11.pdf
*/
        $places = [
            ['name' => 'Phnom Penh', 'code' => '12000'],
            ['name' => 'Kampot', 'code' => ''],
            ['name' => 'Keb', 'code' => ''],
            ['name' => 'Preah Sihanouk', 'code' => '18000'],
            ['name' => 'Kampong Chhnang', 'code' => '04000'],
            ['name' => 'Posat', 'code' => '15000'],
            ['name' => 'Battambong', 'code' => '02000'],
            ['name' => 'Phnom Penh Thmei', 'code' => ''],
            ['name' => 'Pailin', 'code' => ''],
            ['name' => 'Banteay Meanchey', 'code' => ''],
            ['name' => 'Siem Reap', 'code' => '17000'],
            ['name' => 'Kompong Thom', 'code' => '06000'],
            ['name' => 'Kompong Cham', 'code' => '03000'],
            ['name' => 'Prey Veng', 'code' => '14000'],
            ['name' => 'Svay Reang', 'code' => '20000'],
            ['name' => 'Takao', 'code' => ''],
            ['name' => 'Kandal', 'code' => '08000'],
            ['name' => 'Kompong Speu', 'code' => '05000'],
            ['name' => 'Krotie', 'code' => '10000'],
            ['name' => 'Streung Treng', 'code' => '19000'],
            ['name' => 'Mondul Kiri', 'code' => '11000'],
            ['name' => 'Preah Vihea', 'code' => '13000'],
            ['name' => 'Rattanak Kiri', 'code' => '16000'],
            ['name' => 'Koh Kong', 'code' => '09000'],
            ['name' => 'Oudor Meanchey', 'code' => ''],
            ['name' => 'Kompot', 'code' => '07000'],
        ];

        foreach ($places as $place) {
            DB::table('tbl_places')->insert([
                'name' => $place['name'],
                'code' => $place['code'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
