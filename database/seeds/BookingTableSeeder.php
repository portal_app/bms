<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class BookingTableSeeder extends Seeder {

    public function run() {

        for($i=1;$i<=10;$i++) {
            DB::table('tbl_booking')->insert([
                'type_trip' => rand(0,1),
                'branch_id' => rand(0,4),
                'user_id' => rand(0,4),
                'customer_id' => rand(0,4),
                'destination_id' => rand(0,24),
                'bus_id' => rand(0,4),
                'seat_price' => '12321',
                'seat_number' => '1,2,3',
                'seat_amount' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
