<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CustomerTableSeeder extends Seeder
{

    public function run()
    {
        for($i=1; $i<=990; $i++){
            DB::table('tbl_customers')->insert([
                'firstname' => "Ravuthz $i",
                'lastname' => 'Yo',
                'username' => "ravuthz$i",
                'email' =>  "ravuthz$i@gmail.com",
                'phone' => "0964577$i",
                'password' => bcrypt('passcode'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

    }
}
