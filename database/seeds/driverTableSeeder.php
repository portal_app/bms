<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Driver;
class DriverTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Driver::class, 10)->create();

        // for($i=0 ; $i<10 ; $i++){
        //     DB::table('tbl_drivers')->insert([
        //     	'firstname' => str_random(10),
        //     	'lastname' => str_random(10),
        //         'phone' => '0' . rand (10000000 , 99999999),
        //         'email' => str_random(10).'@gmail.com',
        //         'address' => str_random(50),
        //         'card_no' => rand (0000000000 , 9999999999),
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()
        //     ]);
        // }
    }
}
