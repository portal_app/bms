<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TimeTableSeeder extends Seeder {

    public function run() {

       DB::table('tbl_times')->insert([
            'time' => '7:00 AM',
            // 'time_from' => '7:00 AM',
            // 'time_to' => '9:00 AM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '8:00 AM',
            // 'time_from' => '8:00 AM',
            // 'time_to' => '10:00 AM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '9:00 AM',
            // 'time_from' => '9:00 AM',
            // 'time_to' => '11:00 AM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '10:00 AM',
            // 'time_from' => '10:00 AM',
            // 'time_to' => '12:00 AM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '11:00 AM',
            // 'time_from' => '11:00 AM',
            // 'time_to' => '1:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '12:00 PM',
            // 'time_from' => '12:00 PM',
            // 'time_to' => '2:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '1:00 PM',
            // 'time_from' => '1:00 PM',
            // 'time_to' => '3:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '2:00 PM',
            // 'time_from' => '2:00 PM',
            // 'time_to' => '4:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '3:00 PM',
            // 'time_from' => '3:00 PM',
            // 'time_to' => '5:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '4:00 PM',
            // 'time_from' => '4:00 PM',
            // 'time_to' => '6:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_times')->insert([
            'time' => '5:00 PM',
            // 'time_from' => '5:00 PM',
            // 'time_to' => '7:00 PM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

    }
}
