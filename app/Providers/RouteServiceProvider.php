<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider {
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router) {
        parent::boot($router);

        $router->model('customer', 'App\Models\Customer');
        $router->model('branch', 'App\Models\Branch');
        $router->model('time', 'App\Models\Time');
        $router->model('user', 'App\Models\user');
        $router->model('driver', 'App\Models\driver');
        $router->model('bus', 'App\Models\bus');
        $router->model('place', 'App\Models\Place');

    }

    public function map(Router $router) {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
