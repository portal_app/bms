<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];

            case 'POST':
                return [
                    //
                    'firstname' => 'required',
                    'username'  => 'required|unique:tbl_users|min:3',
                    'email'     => 'required|unique:tbl_users',
                    'password'  => 'required|confirmed',
                    'phone'     => 'required|regex:/[0-9]{9,13}/'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    //
                    'firstname' => 'required',
                    'username'  => 'required',
                    'email'     => 'required',
                    'phone'     => 'required|regex:/[0-9]{9,13}/'
                ];
            default:
                break;
        }
/*        return [
            //
            'firstname' => 'required',
            'username'  => 'required|unique:tbl_users|min:3',
            'email'     => 'required|unique:tbl_users',
            'password'  => 'required|confirmed',
            'phone'     => 'required|regex:/[0-9]{9,13}/'
        ];*/
    }


}
