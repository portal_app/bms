<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data['firstname'] = 'required';
        $data['lastname'] = 'required';
        $data['username'] = 'required|unique:tbl_customers|min:3';
        $data['phone'] = 'required|regex:/[0-9]{9,13}/';

        if ($this->is('dash/customer')) {
            $data['email'] = 'required|unique:tbl_customers';
            $data['password']  = 'required|confirmed';
        }

        return $data;
    }
}
