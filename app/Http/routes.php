<?php

// Homepage Routes
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/about', ['as' => 'home.about', 'uses' => 'HomeController@about']);
Route::get('/contact', ['as' => 'home.contact', 'uses' => 'HomeController@contact']);
Route::get('/branches', ['as' => 'home.branches', 'uses' => 'HomeController@branches']);
Route::get('/services', ['as' => 'home.services', 'uses' => 'HomeController@services']);
Route::get('/promotion', ['as' => 'home.promotion', 'uses' => 'HomeController@promotion']);

// Dashboard Routes  http://localhost:8000/feeds/json/
Route::group(['namespace' => 'Feeds', 'prefix' => 'feeds/json'], function () {
    Route::get('users', ['as' => 'feeds.json.users', 'uses' => 'JsonController@users']);
    Route::get('drivers', ['as' => 'feeds.json.drivers', 'uses' => 'JsonController@drivers']);
    Route::get('buses', ['as' => 'feeds.json.buses', 'uses' => 'JsonController@buses']);
    Route::get('times', ['as' => 'feeds.json.times', 'uses' => 'JsonController@times']);
    Route::get('places', ['as' => 'feeds.json.places', 'uses' => 'JsonController@places']);
    Route::get('branches', ['as' => 'feeds.json.branches', 'uses' => 'JsonController@branches']);
    Route::get('customers', ['as' => 'feeds.json.customers', 'uses' => 'JsonController@customers']);
    Route::get('destiantions', ['as' => 'feeds.json.destiantions', 'uses' => 'JsonController@destiantions']);
});

// Authorization Routes
Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
    Route::get('login', ['as' => 'auth.login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth.login', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'AuthController@getLogout']);
    Route::get('register', ['as' => 'auth.register', 'uses' => 'AuthController@getRegister']);
    Route::post('register', ['as' => 'auth.register', 'uses' => 'AuthController@postRegister']);
});

Route::get('dash11', function () {
    return view('dash.admin.index');
});

Route::group(['namespace' => 'Dashboard', 'prefix' => 'dash'], function () {
    // Route::get('/', ['as' => 'dash.index', 'uses' => 'AdminController@index']);

    // Route::resource('/', 'AdminController');
    Route::resource('time', 'TimeController');
    Route::resource('place', 'PlaceController');
    Route::resource('user', 'UserController');
    Route::resource('booking', 'BookingController');
    Route::resource('customer', 'CustomerController');
    Route::resource('branch', 'BranchController');
    Route::resource('driver', 'DriverController');
    Route::resource('bus', 'BusController');
});
