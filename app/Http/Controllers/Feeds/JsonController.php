<?php

namespace App\Http\Controllers\Feeds;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Driver;
use App\Models\Time;
use App\Models\Bus;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\Place;
use App\Models\Destination;

class JsonController extends Controller {

    public function users() {
        return User::all();
    }

    public function drivers() {
        return Driver::all();
    }
    public function buses() {
        return Bus::all();
    }

    public function times() {
        return Time::all();
    }

    public function branches() {
        return Branch::all();
    }

    public function places() {
        return Place::all();
    }

    public function customers() {
        return Customer::all();
    }

    public function destionations() {
        return Destination::all();
    }
}
