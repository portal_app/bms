<?php

namespace App\Http\Controllers\Dashboard;

use JavaScript;
// use App\Http\Requests\BookingRequest;
use App\Models\Bus;
use App\Models\Time;
use App\Models\User;
use App\Models\Place;
use App\Models\Branch;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller {
    public function __construct() {
        $this->middleware('auth');

    }

    public function index(Request $request) {
//        $data['bookingList'] = Booking::latest('created_at')->published()->get();
        //        $bookingList = Booking::where('user_id',$request->user()->id)->get();

        $data['bookingList'] = Booking::where('user_id', $request->user()->id)->get();
        return view('dash.booking.index', $data);
    }

    public function create() {
        $data['branches'] = Branch::lists('name', 'id');
        $data['froms'] = Place::lists('name', 'id');
        $data['tos'] = Place::lists('name', 'id');

        $data['starts'] = Time::lists('time', 'id');
        $data['stops'] = Time::lists('time', 'id');
        $data['price'] = 0;
        $data['buses'] = Bus::lists('code', 'id');
        $data['seat_amounts'] = range(1, 24);
        $data['seats'] = range(1, 24);

        $data['places'] = Place::lists('name', 'id')->toJson();

        JavaScript::put([
            'places' => Place::lists('name', 'id')->toArray()
        ]);

        return view('dash.booking.create', $data);
    }

    public function store(Request $request) {
        dd($request->all());

        /*$user = User::find(1);
        $user->bookings()->create($request->all());
        return redirect(route('dash.booking.index'));*/

        $request->user()->bookings()->create($request->all());
        return redirect(route('dash.booking.index'));

    }

    public function show($id) {
        //
    }

    public function edit($id) {
        return view('dash.booking.edit');
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }
}
