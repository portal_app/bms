<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;

class CustomerController extends Controller {
    public function index() {
        return view('dash.customer.index');
    }

    public function create() {
        return view('dash.customer.create');
    }

    public function store(CustomerRequest $request) {
        $customer = $request->all();
        $customer['password'] = bcrypt($request->password);
        Customer::create($customer);
        return redirect('dash/customer');
    }

    public function show(Customer $customer) {
        $data['customer'] = $customer;
        return view('dash.customer.show', $data);
    }

    public function edit(Customer $customer) {
        $data['customer'] = $customer;
        return view('dash.customer.edit', $data);
    }

    public function update(CustomerRequest $request, Customer $customer) {
        $customer->update($request->all());

        return redirect('dash/customer');
    }

    public function destroy(Customer $customer) {
        $customer->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/customer');
    }
}
