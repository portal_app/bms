<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class AdminController extends Controller {
    public function index() {
        return view('dash.admin.index');
    }

}
