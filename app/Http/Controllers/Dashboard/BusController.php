<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Bus;
use App\Models\Driver;
use Illuminate\Http\Request;
use App\Http\Requests\BusRequest;
use App\Http\Controllers\Controller;

class BusController extends Controller {
    public function index() {
        $data['buses'] = Bus::all();
        return view('dash.bus.index');
    }

    public function create() {
        $data['buses'] = Driver::lists('firstname', 'id');
        return view('dash.bus.create', $data);
    }

    public function store(BusRequest $request) {
        Bus::create($request->all());
        return redirect('dash/bus');
    }

    public function show(Bus $bus) {
        $data['bus'] = $bus;
        $data['driver'] = $bus->driver()->first();
        return view('dash.bus.show', $data);
    }

    public function edit(Bus $bus) {
        $data['bus'] = $bus;
        $data['buses'] = Driver::lists('firstname', 'id');
        return view('dash.bus.edit', $data);
    }

    public function update(BusRequest $request, Bus $bus) {
        $bus->update($request->all());
        return redirect('dash/bus');
    }

    public function destroy(Bus $bus) {
        $bus->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/bus');
    }
}
