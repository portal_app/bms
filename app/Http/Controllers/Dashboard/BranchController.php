<?php

namespace App\Http\Controllers\Dashboard;

//use Illuminate\Http\Request;
// use App\Http\Requests;
use App\Models\User;
use App\Models\Branch;
use App\Http\Controllers\Controller;
use App\Http\Requests\BranchRequest;

class BranchController extends Controller {
    public function index() {
        return view('dash.branch.index');
    }

    public function create() {
        $data['users'] = User::lists('username', 'id');
        return view('dash.branch.create', $data);
    }

    public function store(BranchRequest $request) {
        $branch = $request->all();
        Branch::create($branch);
        return redirect('dash/branch');
    }

    public function show(Branch $branch) {
        $data['branch'] = $branch;
        return view('dash.branch.show', $data);
    }

    public function edit(Branch $branch) {
        $data['users'] = User::lists('username', 'id');
        $data['branch'] = $branch;
        return view('dash.branch.edit', $data);
    }

    public function update(BranchRequest $request, Branch $branch) {
        $branch->update($request->all());

        return redirect('dash/branch');
    }

    public function destroy(Branch $branch) {
        $branch->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/branch');
    }
}
