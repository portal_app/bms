<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DriverRequest;

class DriverController extends Controller {
    public function index() {
        return view('dash.driver.index');
    }

    public function create() {
        return view('dash.driver.create');
    }

    public function store(DriverRequest $request) {
        $driver = $request->all();
        Driver::create($driver);
        return redirect('dash/driver');
    }

    public function show(Driver $driver) {
        return view('dash.driver.show', compact('driver'));
    }

    public function edit(Driver $driver) {
        return view('dash.driver.edit', compact('driver'));
    }

    public function update(DriverRequest $request, Driver $driver) {
        $driver->update($request->all());
        return redirect('dash/driver');
    }

    public function destroy(Driver $driver) {
        $driver->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/driver');
    }
}
