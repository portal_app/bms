<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller {
    public function index() {
        return view('dash.user.index');
    }

    public function create() {
        $data['types'] = User::lists('type', 'type');
        return view('dash.user.create', $data);
    }

    public function store(UserRequest $request) {
        $user = $request->all();
        $user['password'] = bcrypt($request->password);
        User::create($user);
        return redirect('dash/user');
    }

    public function show(User $user) {
        return view('dash.user.show', compact('user'));
    }

    public function edit(User $user) {
        $data['user'] = $user;
        $data['types'] = User::lists('type', 'type');
        switch (User::where('id', $user)->pluck('type')) {
            case 'admin':
                $data['type'] = 'admin';
                break;
            case 'user':
                $data['type'] = 'user';
                break;
            default:
            case 'manager':
                $data['type'] = 'manager';
                break;
        }
        return view('dash.user.edit', $data);
    }

    public function update(UserRequest $request, User $user) {
        $user->update($request->all());
        return redirect('dash/user');
    }

    public function destroy(User $user) {
        $user->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/user');
    }
}
