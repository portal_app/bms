<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Time;
use Illuminate\Http\Request;
use App\Http\Requests\TimeRequest;
use App\Http\Controllers\Controller;

class TimeController extends Controller {
    // view
    public function index() {
        return view('dash.time.index');
    }

    // view
    public function create() {
        return view('dash.time.create');
    }

    // action
    public function store(TimeRequest $request) {
        Time::create($request->all());
        return redirect('dash/time');
    }

    // view detail
    public function show(Time $time) {
        $data['time'] = $time;
        return view('dash.time.show', $data);
    }

    // view
    public function edit(Time $time) {
        $data['time'] = $time;
        return view('dash.time.edit', $data);
    }

    // aciton
    public function update(TimeRequest $request, Time $time) {
        $time->update($request->all());
        return redirect('dash/time');
    }

    // action
    public function destroy(Time $time) {
        $time->delete();
        if (\Request::ajax()) {
            return ['result' => true];
        }
        return redirect('dash/time');
    }
}
