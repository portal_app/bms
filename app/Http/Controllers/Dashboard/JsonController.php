<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

class JsonController extends Controller {
    public function index() {
        return view('json.index');
    }

    public function users() {
        return Datatables::of(User::select('*'))->make(true);
    }

}
