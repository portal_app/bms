<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

use App\Models\Branch;
use App\Models\Place;

class HomeController extends Controller {

    public function __construct() {

    }

    public function index() {
        $data['title'] = 'Home Controller';
        return view('home.index', $data);
    }

    public function branches() {
        $data['title'] = 'Branches';
        $data['branches'] = Branch::all();
        return view('home.branches', $data);
    }

    public function services() {
        $data['title'] = 'Services';

        $data['destinations'] = DB::table('tbl_destinations')
            ->join('tbl_departures', 'tbl_departures.id', '=', 'tbl_destinations.departure_id')
            ->select(
                'tbl_destinations.id', 'tbl_destinations.price',
                'tbl_destinations.place_from', 'tbl_destinations.place_to',
                'tbl_departures.time_start', 'tbl_departures.time_stop'
                )->get();

        return view('home.services', $data);
    }

    public function promotion() {
        $data['title'] = 'Promotion';
        // $data['promotion'] = Promotion::all()->toArray();
        return view('home.promotion', $data);
    }

    public function about() {
        $data['title'] = 'About';
        return view('home.about', $data);
    }

    public function contact() {
        $data['title'] = 'Contact';
        return view('home.contact', $data);
    }




}
