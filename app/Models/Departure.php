<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departure extends Model {
    public $table = 'tbl_departures';

    public function time_start() {
    	return $this->hasOne('App\Models\Time', 'id', 'time_start');
    }

    public function time_stop() {
    	return $this->hasOne('App\Models\Time', 'id', 'time_stop');
    }


}
