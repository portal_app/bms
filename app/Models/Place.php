<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model {
    public $table = 'tbl_places';

    protected $fillable = [
        'name',
        'code'
    ];
}
