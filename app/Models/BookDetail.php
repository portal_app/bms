<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookDetail extends Model {
    public $table = 'tbl_bookdetails';
}
