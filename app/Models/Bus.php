<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    public $table = 'tbl_buses';

    protected $fillable = [
        'code',
        'driver_id',
        'park',
        'type'
    ];

    public function driver(){
        return $this->belongsTo('App\Models\driver');
    }
}
