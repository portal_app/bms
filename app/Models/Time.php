<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Time extends Model {
    public $table = 'tbl_times';

    protected $fillable = [
        'time'
    ];    
}
