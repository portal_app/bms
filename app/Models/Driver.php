<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public $table = 'tbl_drivers';

    protected $fillable = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'address',
        'card_no'
    ];


    public function buses(){
        return $this->hasMany('App\Models\Bus');
    }
}
