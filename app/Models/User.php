<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract {
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'tbl_users';

    protected $fillable = [
        'firstname',
        'lastname',
        'username',
        'email',
        'type',
        'phone',
        'address',
        'card_no',
        'password'
    ];

    protected $hidden = ['password', 'remember_token', '_method'];

    public function branch() {
        return $this->belongTo('App\Models\Branch');
    }

    public function bookings() {
        return $this->hasMany('App\Models\Booking');
    }

}
