<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

    public $table = 'tbl_branches';

    protected $fillable = [
        'name',
        'address',
        'control_by',
        'phone'
    ];

    public function users() {
        return $this->hasMany('App\Models\User');
    }

}
