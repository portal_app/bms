@extends('home')
@section('content')
    <section>
        <div class="container">
            <h2>Bus Reservation Servive</h2>
            <p>In our company we there is a service to transfer the customers from Phnom Penh central to other provinces, or from each province to Phnom Penh.</p>
                <p>you can click on destination to book ticket here</p>

            <div class="row">

                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>From</th>
                                        <th></th>
                                        <th>To</th>
                                        <th>Price</th>
                                        <th>Departure</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($destinations as $dest)
                                        <tr>
                                            <td>{{$dest->place_from}}</td>
                                            <td class="text-center"><span class='glyphicon glyphicon-transfer' aria-hidden='true'></span></td>
                                            <td>{{$dest->place_to}}</td>
                                            <td>{{$dest->price}}</td>
                                            <td>{{$dest->time_start}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <h3>Other Infomation</h3>
                </div>

            </div>

            <div class="clearfix"></div>
        </div>
    </section>
@stop
@section('bottom_content')
@stop
