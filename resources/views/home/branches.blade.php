@extends('home')

@section('content')

    <section>
        <div class="container">
            <h2>Brands</h2>

            <p>In our company we there is a service to transfer the customers from Phnom Penh central to other provinces, or from each province to Phnom Penh.</p>


            <div class="row">

                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Branch</th>
                                        <th>Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($branches as $branch)
                                        <tr>
                                            <td>
                                                <p>{{$branch->name}}</p>
                                            </td>
                                            <td>
                                                <p>{{$branch->address}}</p>
                                                <p>Phone : {{$branch->phone}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <h3>Other Infomation</h3>
                </div>

            </div>

            <div class="clearfix"></div>
        </div>
    </section>

@stop

@section('bottom_content')

@stop
