@extends('home')

@section('content')
    <section>
        <div class="container">
            <div class="slider">
                <ul class="rslides" id="slider1">
                    <li><img src="images/banner/2.jpg" alt=""></li>
                    <li><img src="images/banner/1.jpg" alt=""></li>
                </ul>
            </div>

            <h2>Welcome to BMS</h2>

            <div class="row">
                <div class="col-md-8">
                    <hr/><p>
                        <span class="glyphicon glyphicon-hand-right"></span> BMS(Bus Management System) is the online bus booking ticket services in Cambodia!<br/></p>
                        <p>
                            You can booking the bus ticket for going every provinces in Cambodia. We have lots of Branches for supporting our services to all the customers.
                            All customers can pre-booking the ticket here. You book both the One-Way or Round-Trip ticket as your choise.
                        </p>
                        <p></p>
                </div>
                <div class="col-md-4">
                    test
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </section>
@stop

@section('bottom_content')

@stop
