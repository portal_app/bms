@extends('dash')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Places{!! link_to_route('dash.place.create', 'New Place', null, ['class' => 'btn btn-default pull-right']) !!}</h1>
        </div>
    </div>
<div class="panel panel-default">
<div class="panel-heading">
    List places
</div>
<div class="panel-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="placeTable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Place</th>
                    <th>Code</th>
                    <th>Create At</th>
                    <th>Update At</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>
    <hr/><br/>
@stop


@push('scripts')
    <script>
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
	</script>

    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}
    {!! HTML::script('dash/js/place.js') !!}
@endpush

