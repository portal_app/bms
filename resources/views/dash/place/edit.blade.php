@extends('dash')

@section('content')
    @include('errors.validate')
    {!! Form::model($place, ['method' => 'PATCH', 'action' => ['Dash\PlaceController@update', $place->id]]) !!}
    	@include('dash.place.form', ['submitText' => 'Update Place', 'pageHeader' => 'Update Place Data'])
    {!! Form::close() !!}
@stop


