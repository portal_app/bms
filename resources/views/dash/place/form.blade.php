
<div class="form-group row">
	<div class="col-md-6">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		{!! Form::label('code', 'Code') !!}
		{!! Form::text('code', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group row">
	<button type='submit' class='btn btn-primary' id="submitPlace">{{ $submitText }}</button>
</div>
