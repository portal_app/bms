@extends('dash')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1>View Place</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="update_at">Name: </label>
                            <p class="form-control">{{ $place->name }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="update_at">Code: </label>
                            <p class="form-control">{{ $place->code }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="created_at">Created: </label>
                            <p class="form-control">{{ $place->created_at }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="update_at">Updated: </label>
                            <p class="form-control">{{ $place->updated_at }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.place.edit', 'UPDATE', $place->id, ['class' => 'btn btn-block btn-info']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                                {!! Form::open(['method'=>'DELETE','route'=>['dash.place.destroy',$place->id]])!!}
                                    <div class="form-group">
                                        {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                                    </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.place.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

