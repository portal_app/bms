@extends('dash')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create Drivers</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    {!! Form::open(['route' => 'dash.time.store']) !!}
                    	@include('dash.time.form', ['submitText' => 'Add'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection