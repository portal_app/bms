@extends('dash')

@section('content')
    @include('errors.validate')
    {!! Form::model($time, ['method' => 'PATCH', 'action' => ['Dash\TimeController@update', $time->id]]) !!}

    	@include('dash.time.form', ['submitText' => 'Update Time','pageHeader'=>'Update Time data!'])
    {!! Form::close() !!}
@stop


