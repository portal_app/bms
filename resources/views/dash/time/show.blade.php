@extends('dash')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1>View Time</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="update_at">Updated: </label>
                            <p class="form-control">{{ $time->time }}</p>
                        </div>
                    </div>

                    <div class="row">  
                        <div class="form-group col-md-12">
                            <label for="created_at">Created: </label>
                            <p class="form-control">{{ $time->created_at }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="update_at">Updated: </label>
                            <p class="form-control">{{ $time->updated_at }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group"> 
                                {!! link_to_route('dash.time.edit', 'UPDATE', $time->id, ['class' => 'btn btn-block btn-info']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                                {!! Form::open(['method'=>'DELETE','route'=>['dash.time.destroy',$time->id]])!!}
                                    <div class="form-group">    
                                        {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                                    </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">   
                                {!! link_to_route('dash.time.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('scripts')

    {!! HTML::style('dash/css/bootstrap-clockpicker.min.css') !!}
    {!! HTML::script('dash/js/bootstrap-clockpicker.min.js') !!}

    <script type="text/javascript">
        $('.clockpicker').clockpicker({
            placement: 'bottom',
            align: 'left'
        });
    </script>

@endpush
