@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$user->username}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">First Name: </label>
                            <p class="form-control">{{ $user->firstname }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="firstname">Last Name: </label>
                            <p class="form-control">{{ $user->lastname }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id" class="text-danger">*Username: </label>
                            <p class="form-control">{{ $user->username }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname" class="text-danger">*Email: </label>
                            <p class="form-control">{{ $user->email }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Phone: </label>
                            <p class="form-control">{{ $user->phone }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="id">Address: </label>
                            <textarea class="form-control" rows="5" readonly="readonly">{{ $user->address }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Card_No: </label>
                            <p class="form-control">{{ $user->card_no }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Type: </label>
                            <p class="form-control">{{ $user->type }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Created_at: </label>
                            <p class="form-control">{{ $user->created_at }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Updated_at: </label>
                            <p class="form-control">{{ $user->updated_at }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.user.edit', 'UPDATE', $user->id, ['class' => 'btn btn-block btn-info']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                                {!! Form::open(['method'=>'DELETE','route'=>['dash.user.destroy',$user->id]])!!}
                            <div class="form-group">
                                {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                            </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.user.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
            <hr/><br/>
@stop
