@extends('dash')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Drivers{!! link_to_route('dash.driver.create', 'New Driver', null, ['class' => 'btn btn-default pull-right']) !!}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All driver data list!
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="DriverTable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/><br/>
@stop

@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}
    {!! HTML::script('dash/js/driver.js') !!}
@endpush
