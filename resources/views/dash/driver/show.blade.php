@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$driver->firstname}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">First Name: </label>
                            <p class="form-control">{{ $driver->firstname }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="firstname">Last Name: </label>
                            <p class="form-control">{{ $driver->lastname }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Phone: </label>
                            <p class="form-control">{{ $driver->phone }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Email: </label>
                            <p class="form-control">{{ $driver->email }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="id">Address: </label>
                            <textarea class="form-control" rows="5" readonly="readonly">{{ $driver->address }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Created_at: </label>
                            <p class="form-control">{{ $driver->created_at }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Updated_at: </label>
                            <p class="form-control">{{ $driver->updated_at }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.driver.edit', 'UPDATE', $driver->id, ['class' => 'btn btn-block btn-info']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                                {!! Form::open(['method'=>'DELETE','route'=>['dash.driver.destroy',$driver->id]])!!}
                            <div class="form-group">
                                {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                            </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.driver.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
            <hr/><br/>
@stop
