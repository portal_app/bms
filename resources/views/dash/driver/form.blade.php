<div class="form-group">
    {!! Form::label('firstname','First Name') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example Vichet.</p>
</div>
<div class="form-group">
    {!! Form::label('lastname','Last Name') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example Sai.</p>
</div>
<div class="form-group">
    {!! Form::label('phone','Phone') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('email','Email') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example email@example.com</p>
</div>

<div class="form-group">
    {!! Form::label('address','Address') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('card_no','Card Number') !!}
    {!! Form::text('card_no', null, ['class' => 'form-control']) !!}
    <p class="help-block">Identify Card.</p>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit($submittype, ['class' => 'btn btn-block btn-primary']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! link_to_route('dash.driver.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
        </div>
    </div>
</div>
