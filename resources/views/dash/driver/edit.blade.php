@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Driver</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($driver, ['method' => 'PATCH', 'action' => ['Dash\DriverController@update', $driver->id]])!!}
                        @include('dash.driver.form', ['submittype' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <hr/><br/>
@stop
