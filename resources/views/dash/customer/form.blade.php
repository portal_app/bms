<div class="form-group">
    {!! Form::label('firstname','First Name') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('lastname','Last Name') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('username','Username') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('email','Examplemail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('phone','Phone') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit($submittype, ['class' => 'btn btn-primary form-control']) !!}
