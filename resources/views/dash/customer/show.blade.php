@extends('dash')


@section('content')
    <div class="row">
        <div class="col-md-8">

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="firstname">First Name: </label>
                    <p class="form-control">{{ $customer->firstname }}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="lastname">Last Name: </label>
                    <p class="form-control">{{ $customer->lastname }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="username">User Name: </label>
                    <p class="form-control">{{ $customer->username }}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email: </label>
                    <p class="form-control">{{ $customer->email }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone: </label>
                    <p class="form-control">{{ $customer->phone }}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="created_at">Created: </label>
                    <p class="form-control">{{ $customer->created_at }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="update_at">Updated: </label>
                    <p class="form-control">{{ $customer->updated_at }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! link_to_route('dash.customer.edit', 'UPDATE', $customer->id, ['class' => 'btn btn-block btn-info']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                        {!! Form::open(['method'=>'DELETE','route'=>['dash.customer.destroy',$customer->id]])!!}
                    <div class="form-group">
                        {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                    </div>
                        {!! Form::close()!!}
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! link_to_route('dash.customer.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <hr/><br/>
@stop
