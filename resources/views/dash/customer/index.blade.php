@extends('dash')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Customer Information{!! link_to_route('dash.customer.create', 'create', null, ['class' => 'btn btn-default pull-right']) !!}</h1>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            List customers
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="customerTable">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}
    {!! HTML::script('dash/js/customer.js') !!}
@endpush
