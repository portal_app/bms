@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Customer</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($customer, ['method' => 'PATCH', 'action' => ['Dash\CustomerController@update', $customer->id]])!!}
                        @include('dash.customer.form', ['submittype' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <hr/><br/>
@stop
