@extends('dash')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Booking List</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All booking data list!
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>User ID</th>
                                    <th>Branch ID</th>
                                    <th>Time ID</th>
                                    <th>Customer ID</th>
                                    <th>Trip Type</th>
                                    <th>Place From</th>
                                    <th>Place To</th>
                                    <th>Seat Amout</th>
                                    <th>Seat Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bookingList as $booking)
                                    <tr class="odd gradeX">
                                        <td>{{ $booking->id }}</td>
                                        <td>{{ $booking->user_id }}</td>
                                        <td>{{ $booking->branch_id }}</td>
                                        <td>{{ $booking->time_id }}</td>
                                        <td>{{ $booking->customer_id }}</td>
                                        <td>{{ $booking->type_trip }}</td>
                                        <td>{{ $booking->place_from }}</td>
                                        <td>{{ $booking->place_to }}</td>
                                        <td>{{ $booking->seats_amount }}</td>
                                        <td>{{ $booking->seats_price }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr/><br/>

@endsection

@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {{-- {!! HTML::style('dash/css/datatables.css') !!} --}}
    {{-- {!! HTML::script('dash/js/datatables.js') !!} --}}
    {!! HTML::script('dash/js/booking.js') !!}
@endpush