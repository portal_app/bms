@extends('dash')

@section('content')

    {!! Form::open(['route'=>'dash.booking.store', 'id'=>'frmBooking']) !!}

        @include('dash.booking.form',['submitButtonText'=>'Booking!','pageHeader'=>'Book the ticket!'])

    {!! Form::close() !!}

@stop