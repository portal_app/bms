<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{!! $pageHeader !!}</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel-body">

            @include('dash.common.errors')

            <div class="row">
                <div class="col-lg-8">

                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('trip_type','Trip Types') !!}
                                {!! Form::select('trip_type', ['One Way', 'Round Trip'], null,['id'=>'selFrom','class'=>'form-control']) !!}
                           </div>
                            <div class="col-md-6">
                                <label>Date</label>
                                <p class="form-control">
                                    {{ date('D d-m-Y') }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col-md-6">
                                {!! Form::label('place_from','Dest') !!}
                                {!! Form::select('place_from',$froms,null,['id'=>'selFrom','class'=>'form-control']) !!}
                           </div>
                            <div class="col-md-6">
                                {!! Form::label('place_to','To') !!}
                                {!! Form::select('place_to',$tos,null,['id'=>'selTo','class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                           {!! Form::label('bus_id','Bus') !!}
                           {!! Form::select('bus_id',$buses,null,['id'=>'bus_id','class'=>'form-control']) !!}
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                   {!! Form::label('price','Price') !!}
                                   {!! Form::text('price', '0', ['id' => 'txtPrice', 'class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                   {!! Form::label('seat_no','Amount') !!}
                                   {!! Form::text('seat_no', '0', ['id' => 'txtAmount', 'class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                   {!! Form::label('total','Total') !!}
                                   {!! Form::text('total', '0', ['id' => 'txtTotal', 'class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                @foreach ($seats as $seat)
                                    <div class="col-md-1">
                                        <label class="checkbox-inline"><input type="checkbox" name="seat_id" value="{{ $seat }}">{{ $seat }}</label>
                                    </div>
                                @endforeach
                           </div>
                        </div>

                        <div class="form-group">
                            {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary']) !!}
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr/><br/>

@push('scripts')
    <script type="text/javascript">
        var places = {!! $places !!};

        console.log(places);
    </script>
    {!! HTML::script('dash/js/booking.js') !!}
@endpush