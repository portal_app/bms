@extends('dash')


@section('content')


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Branch</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($branch, ['method' => 'PATCH', 'action' => ['Dash\BranchController@update', $branch->id]])!!}
                        @include('dash.branch.form', ['submitText' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <hr/><br/>
@stop
