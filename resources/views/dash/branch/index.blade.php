@extends('dash')

@section('title', 'Admin')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Branches Information{!! link_to_route('dash.branch.create', 'create', null, ['class' => 'btn btn-default pull-right']) !!}</h1>
        </div>
    </div>
<div class="panel panel-default">
<div class="panel-heading">
    List branches
</div>
<div class="panel-body">
    <div class="table-responsive">
        <table id="tblBranches" class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Controler</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>
    <hr/><br/>
@stop
@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}
    {!! HTML::script('dash/js/branch.js') !!}
@endpush