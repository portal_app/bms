@extends('dash')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Branch detail!</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            <div class="row">
                
                <div class="form-group col-md-6">
                    <label for="lastname">Branch Name: </label>
                    <p class="form-control">{{ $branch->name }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone: </label>
                    <p class="form-control">{{ $branch->phone }}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email: </label>
                    <p class="form-control">{{ $branch->control_by }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="username">Address: </label>
                        {!! Form::textarea('address', $branch->address, ['class'=>'form-control','readonly'=>'readonly']) !!}
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="created_at">Created: </label>
                    <p class="form-control">{{ $branch->created_at }}</p>
                </div>
                <div class="form-group col-md-6">
                    <label for="update_at">Updated: </label>
                    <p class="form-control">{{ $branch->updated_at }}</p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group"> 
                        {!! link_to_route('dash.branch.edit', 'UPDATE', $branch->id, ['class' => 'btn btn-block btn-info']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                        {!! Form::open(['method'=>'DELETE','route'=>['dash.branch.destroy',$branch->id]])!!}
                    <div class="form-group">    
                        {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                    </div>
                        {!! Form::close()!!}
                </div>
                <div class="col-md-4">
                    <div class="form-group">   
                        {!! link_to_route('dash.branch.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <hr/><br/>
@stop