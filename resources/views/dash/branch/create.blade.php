@extends('dash')

@section('title', 'Create Branch')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Insert Branch Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::open(['route' => 'dash.branch.store']) !!}
                        @include('dash.branch.form',  ['submitText' => 'Create Branch'])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <hr/><br/>
@stop