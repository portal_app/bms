<div class="form-group">
    {!! Form::label('code','Plate Number') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
    <p class="help-block">Example Vichet.</p>
</div>

<div class="form-group">
    {!! Form::label('driver_id','Driver Name') !!}
    {!! Form::select('driver_id', $buses, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('park','Park') !!}
    {!! Form::text('park', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('type','Type') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit($submittype, ['class' => 'btn btn-block btn-primary']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! link_to_route('dash.bus.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
        </div>
    </div>
</div>
