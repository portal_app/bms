@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$bus->firstname}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Plate Number: </label>
                            <p class="form-control">{{ $bus->code }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="firstname">Driver Name: </label>
                            <p class="form-control">{{ $driver->firstname }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Park: </label>
                            <p class="form-control">{{ $bus->park }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Type: </label>
                            <p class="form-control">{{ $bus->type }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id">Created_at: </label>
                            <p class="form-control">{{ $bus->created_at }}</p>
                        </div>
                            <div class="form-group col-md-6">
                            <label for="firstname">Updated_at: </label>
                            <p class="form-control">{{ $bus->updated_at }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.bus.edit', 'UPDATE', $bus->id, ['class' => 'btn btn-block btn-info']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                                {!! Form::open(['method'=>'DELETE','route'=>['dash.bus.destroy',$bus->id]])!!}
                            <div class="form-group">
                                {!! Form::submit('DELETE',['class' => 'btn btn-block btn-danger'])!!}
                            </div>
                                {!! Form::close()!!}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! link_to_route('dash.bus.index', 'CANCEL', null, ['class' => 'btn btn-block btn-default']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
            <hr/><br/>
@stop
