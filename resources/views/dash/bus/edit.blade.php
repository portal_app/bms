@extends('dash')


@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Bus</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($bus, ['method' => 'PATCH', 'action' => ['Dash\BusController@update', $bus->id]])!!}
                        @include('dash.bus.form', ['submittype' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <hr/><br/>
@stop
