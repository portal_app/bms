<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create Drivers</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-8">
                @include('errors.validate') {!! Form::open(['route' => 'dash.bus.store']) !!} @include('dash.bus.form', ['submittype' => 'Add']) {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<hr/>
<br/>
