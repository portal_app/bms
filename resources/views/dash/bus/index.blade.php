@extends('dash')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Buses{!! link_to_route('dash.bus.create', 'New Bus', null, ['class' => 'btn btn-default pull-right']) !!}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All user data list!
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="BusTable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Bus Plate</th>
                                    <th>Driver Name</th>
                                    <th>Park</th>
                                    <th>Type</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/><br/>
@stop

@push('scripts')
    <script type="text/javascript">
        var _link = '{{ url() }}/';
        var _code = '{{ csrf_token() }}';
    </script>

    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}
    {!! HTML::script('dash/js/bus.js') !!}
@endpush
