<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Auth - @yield('title')</title>

    {!! HTML::style('dash/css/bootstrap.min.css') !!}


    <style type="text/css">
    	.page-wrapper {
    		margin-top: 100px;
    	}
        * {
            border-radius: 1px !important;

        }
    </style>
</head>

<body>
    <div class="page-wrapper">
        @yield('content')
    </div>
</body>
</html>

